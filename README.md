# YLab: Система переводов

Привет. Вы смотрите результат моего выполнения тестового задания для вакансии <https://career.habr.com/vacancies/1000055915>

## Стек технологий

- Python 3.8
- Django 3.0
- PostgreSQL
- Redis
- Celery
- Docker+Docker Compose

## Запуск

Для того, чтобы запустить проект, необходим установленный и запущенный Docker свежей версии, а так же docker-compose.

Запуск производится следующей командой:
```bash
docker-compose up -d
```

## Использование API

В примерах будет использоваться утилита httpie, инструкция по установке: <https://httpie.org/#installation>

При это, можно использовать любой другой инструмент для выполнения запросов.

### Регистрация пользователя

Запрос:
```bash
http --json POST "localhost/api/auth/signup/" username=user1@example.org balance=100 currency=BTC password=qazwsx24
```

Пример ответа:
```text
HTTP/1.1 201 Created
Allow: POST, OPTIONS
Connection: keep-alive
Content-Length: 39
Content-Type: application/json
Date: Wed, 29 Jan 2020 03:01:32 GMT
Server: nginx/1.17.8
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "balance": "100.0000",
    "currency": "BTC"
}
```


### Аутентификация

Запрос:
```bash
http --json POST localhost/api/auth/login/ username=user1@example.org password=qazwsx24
```

Пример ответа:
```text
HTTP/1.1 200 OK
Allow: POST, OPTIONS
Connection: keep-alive
Content-Length: 52
Content-Type: application/json
Date: Wed, 29 Jan 2020 03:02:32 GMT
Server: nginx/1.17.8
Vary: Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "token": "5a6139f0e27b7938a4051f303a5127c7a8ea9c39"
}
```

**Получив токен, можно получить доступ к приватному апи: просмотр баланса, операций, создание перевода.**

Токен следуется передавать в заголовке `Authorization` как показано в последующих примерах.

### Просмотр баланса

Запрос:
```bash
http --json GET "localhost/api/account/" Authorization:"Token 5a6139f0e27b7938a4051f303a5127c7a8ea9c39"
```

Пример ответа:
```text
HTTP/1.1 200 OK
Allow: GET, HEAD, OPTIONS
Connection: keep-alive
Content-Length: 39
Content-Type: application/json
Date: Wed, 29 Jan 2020 03:04:51 GMT
Server: nginx/1.17.8
Vary: Accept
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "balance": "100.0000",
    "currency": "BTC"
}
```

### Выполнение перевода

Запрос:
```bash
http --json POST "localhost/api/operation/"  Authorization:"Token 5a6139f0e27b7938a4051f303a5127c7a8ea9c39" to_user=user2@example.org amount=1
```

Пример ответа:
```text
HTTP/1.1 201 Created
Allow: GET, POST, HEAD, OPTIONS
Connection: keep-alive
Content-Length: 81
Content-Type: application/json
Date: Wed, 29 Jan 2020 03:05:52 GMT
Server: nginx/1.17.8
Vary: Accept
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "amount": "1.0000",
    "currency": "BTC",
    "from_user": "user1@example.org",
    "to_user": "user2@example.org"
}
```

### Просмотр операций по счету

Запрос:
```bash
http --json GET "localhost/api/operation/"  Authorization:"Token 5a6139f0e27b7938a4051f303a5127c7a8ea9c39"
```

Пример ответа:
```text
HTTP/1.1 200 OK
Allow: GET, POST, HEAD, OPTIONS
Connection: keep-alive
Content-Length: 133
Content-Type: application/json
Date: Wed, 29 Jan 2020 03:12:37 GMT
Server: nginx/1.17.8
Vary: Accept
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "amount": "1.0000",
            "currency": "BTC",
            "from_user": "user1@example.org",
            "to_user": "user2@example.org"
        }
    ]
}
```

В этом методе API поддерживается пагинация при помощи GET-парметров `limit` и `offset`

## Anything else?

- Пришлось применить сторонный апи для получения курса BTС
- Некоторые требования не понял, например указание начального баланса при регистрации. Решил считать допущением в рамках тествого задания, ну либо это АПИ изначально приватное все.
- Код покрыт тестами, но как известно, тестов много не бывает. Есть огромное количество всяких штук, которые можно попроверять еще.
- На выполнение задания по моим подсчетам ушло не больше 6 часов.