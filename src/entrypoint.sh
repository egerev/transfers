#!/usr/bin/env sh
set -e

until PGPASSWORD=$POSTGRES_PASSWORD psql -h "db" -U "$POSTGRES_USER" -c '\q'; do
  echo "Postgres is unavailable - sleeping"
  sleep 1
done

echo "Postgres is up"

until redis-cli -h redis -p 6379 ping | grep PONG; do
    sleep 1
    echo "Retry Redis ping... "
done

echo "Redis is up"

cmd="$@"

if [ "$1" == "prepare" ]; then
    echo "Applying migrations"
    python manage.py migrate

    echo "Collecting static"
    python manage.py collectstatic --noinput

    shift 1
    cmd="$@"
fi

exec $cmd