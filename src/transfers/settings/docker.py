from .base import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "transfers",
        "USER": "transfers",
        "PASSWORD": "ieth0Vin",
        "HOST": "db",
        "PORT": "5432",
        "ATOMIC_REQUESTS": True,
    },
}

STATIC_ROOT = "/usr/share/static/"