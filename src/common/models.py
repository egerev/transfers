from django.db import models


class CurrencyAwareModelMixin(models.Model):
    CURRENCY_EUR = "EUR"
    CURRENCY_USD = "USD"
    CURRENCY_GPB = "GBP"
    CURRENCY_RUB = "RUB"
    CURRENCY_BTC = "BTC"
    CHOICES_CURRENCY = (
        (CURRENCY_EUR, "Евро"),
        (CURRENCY_USD, "Доллар США"),
        (CURRENCY_GPB, "Фунт стерлингов"),
        (CURRENCY_RUB, "Российский рубль"),
        (CURRENCY_BTC, "Биткоин"),
    )
    CURRENCIES = [CURRENCY_EUR, CURRENCY_USD, CURRENCY_GPB, CURRENCY_RUB, CURRENCY_BTC]

    currency = models.CharField(
        verbose_name="Валюта", max_length=3, choices=CHOICES_CURRENCY
    )

    class Meta:
        abstract = True
