import uuid

from django.contrib.auth import get_user_model


def create_and_return_user():
    """Creates new user account with random email and password qwerty, than returns user object."""
    User = get_user_model()
    u = User(email="{}@example.org".format(uuid.uuid4().hex))
    u.set_password("qwerty")
    u.save()
    return u
