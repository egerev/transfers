from django.test import TestCase

from common.utils.tests import create_and_return_user
from ..models import Account


class TestAccountModelMethods(TestCase):
    def test_create_account(self):
        user = create_and_return_user()

        account = Account.objects.create(user=user, currency=Account.CURRENCY_BTC)
        print(str(account))
