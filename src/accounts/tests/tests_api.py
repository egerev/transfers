import uuid

from django.contrib.auth import get_user_model
from django.test import TestCase, Client

User = get_user_model()


class TestAuthSignup(TestCase):
    def setUp(self):
        self.client = Client()

    def test_signup(self):
        response = self.client.post("/api/auth/signup/", data={
            "username": "test@example.org",
            "password": "qwerty",
            "currency": "BTC",
            "balance": 100500
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post("/api/auth/signup/", data={
            "username": "test@example.org",
            "password": "qwerty",
            "currency": "BTC",
            "balance": 100500
        })
        self.assertEqual(response.status_code, 400)
        error_in_username = "username" in response.json()
        self.assertTrue(error_in_username)

    def test_signup_with_empty_password(self):
        response = self.client.post("/api/auth/signup/", data={
            "username": "test_empty_password@example.org",
            "password": "",
            "currency": "BTC",
            "balance": 100500
        })
        self.assertEqual(response.status_code, 400)
        error_in_password = "password" in response.json()
        self.assertTrue(error_in_password)

    def test_signup_with_unknown_currency(self):
        response = self.client.post("/api/auth/signup/", data={
            "username": "test_unknown_currency@example.org",
            "password": "",
            "currency": "PRB",
            "balance": 100500
        })
        self.assertEqual(response.status_code, 400)
        error_in_currency = "currency" in response.json()
        self.assertTrue(error_in_currency)


class TestAuthLogin(TestCase):
    def setUp(self):
        self.username = "test_login@example.org"
        self.password = "qwerty"
        response = self.client.post("/api/auth/signup/", data={
            "username": self.username,
            "password": self.password,
            "currency": "BTC",
            "balance": 100500
        })
        self.assertEqual(response.status_code, 201)

    def test_login(self):
        response = self.client.post("/api/auth/login/", data={
            "username": self.username,
            "password": self.password,
        }, content_type="application/json")
        data = response.json()
        print(data)
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(data.get("token"))


class BaseApiTestCase(TestCase):
    def create_and_return_user_with_token(self):
        username = uuid.uuid4().hex + "@example.org"
        password = uuid.uuid4().hex
        response = self.client.post("/api/auth/signup/", data={
            "username": username,
            "password": password,
            "currency": "BTC",
            "balance": 100500
        }, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        response = self.client.post("/api/auth/login/", data={
            "username": username,
            "password": password,
        }, content_type="application/json")
        token = response.json()["token"]

        return User.objects.get(email=username), token


class TestAccountApi(BaseApiTestCase):
    def test_account(self):
        _, token = self.create_and_return_user_with_token()
        client = Client(HTTP_AUTHORIZATION='Token {}'.format(token))
        response = client.get("/api/account/")
        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data["currency"], "BTC")
        self.assertEqual(data["balance"], "100500.0000")


class TestOperationApi(BaseApiTestCase):
    def test_transfer(self):
        user_to, _ = self.create_and_return_user_with_token()
        user_from, token = self.create_and_return_user_with_token()

        client = Client(HTTP_AUTHORIZATION='Token {}'.format(token))
        response = client.post("/api/operation/", data={"to_user": user_to.email, "amount": 1.0})
        data = response.json()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(data["amount"], "1.0000")

        response = client.get("/api/account/")
        data = response.json()
        self.assertEqual(data["balance"], "100499.0000")

    def test_transfer_too_much(self):
        user_to, _ = self.create_and_return_user_with_token()
        user_from, token = self.create_and_return_user_with_token()

        client = Client(HTTP_AUTHORIZATION='Token {}'.format(token))
        response = client.post("/api/operation/", data={"to_user": user_to.email, "amount": "100600.0"})
        data = response.json()
        self.assertEqual(response.status_code, 400)
        error_in_amount = "amount" in data
        self.assertTrue(error_in_amount)

    def test_get_operations(self):
        user_to, _ = self.create_and_return_user_with_token()
        user_from, token = self.create_and_return_user_with_token()

        client = Client(HTTP_AUTHORIZATION='Token {}'.format(token))
        response = client.post("/api/operation/", data={"to_user": user_to.email, "amount": 1.0})
        self.assertEqual(response.status_code, 201)

        response = client.get("/api/operation/")
        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 1)


