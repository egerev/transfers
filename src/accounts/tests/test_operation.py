from django.core.exceptions import ValidationError
from django.test import TestCase

from common.utils.tests import create_and_return_user
from ..models import Account, Operation


class TestAccountModelMethods(TestCase):
    def setUp(self):
        self.user1 = create_and_return_user()
        Account.objects.create(user=self.user1, currency=Account.CURRENCY_BTC, balance=100)

        self.user2 = create_and_return_user()
        Account.objects.create(user=self.user2, currency=Account.CURRENCY_BTC)

    def test_create_operation(self):
        operation = Operation.objects.create(
            from_account=self.user1.account, to_account=self.user2.account, amount=1
        )
        print(str(operation))
        operation.full_clean()

    def test_operation_to_self(self):
        operation = Operation(
            from_account=self.user1.account, to_account=self.user1.account, amount=1
        )

        with self.assertRaises(ValidationError):
            operation.full_clean()

    def test_operation_with_zero_amount(self):
        operation = Operation(
            from_account=self.user1.account, to_account=self.user2.account, amount=0
        )

        with self.assertRaises(ValidationError):
            operation.full_clean()

    def test_operation_with_too_much_money(self):
        operation = Operation(
            from_account=self.user1.account, to_account=self.user2.account, amount=1000
        )

        with self.assertRaises(ValidationError):
            operation.full_clean()

    def test_operation_with_negative_amount(self):
        operation = Operation(
            from_account=self.user1.account, to_account=self.user2.account, amount=-10
        )

        with self.assertRaises(ValidationError):
            operation.full_clean()

    def test_operation_without_receiver(self):
        operation = Operation(
            from_account=self.user1.account, amount=10
        )

        with self.assertRaises(ValidationError):
            operation.full_clean()

    def test_not_save_operation(self):
        operation = Operation(
            from_account=self.user1.account, to_account=self.user2.account, amount=1
        )
        print(str(operation))

    def test_transfer(self):
        Operation.transfer(self.user1.account, self.user2.account, 10)

