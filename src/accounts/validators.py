from django.core.exceptions import ValidationError


def validate_amount(value):
    if value == 0:
        raise ValidationError("Сумма перевода должна быть отличной от нуля")

    if value < 0:
        raise ValidationError("Сумма перевода не может быть отрицательной")
