from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Account, Operation

User = get_user_model()


class NewAccountSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.EmailField(write_only=True)
    password = serializers.CharField(max_length=255, write_only=True)

    class Meta:
        model = Account
        fields = ["username", "password", "currency", "balance"]

    def validate_username(self, value):
        try:
            User.objects.get(email=value.lower())
        except User.DoesNotExist:
            return value.lower()
        raise serializers.ValidationError(
            "Пользователь с таким адресом электронной почты уже существует"
        )

    def create(self, validated_data):
        print(validated_data)
        user = User.objects.create(email=validated_data["username"])
        user.set_password(validated_data["password"])
        user.save()

        account = Account.objects.create(
            user=user,
            currency=validated_data["currency"],
            balance=validated_data["balance"],
        )
        return account


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Account
        fields = ["currency", "balance"]


class OperationSerializer(serializers.HyperlinkedModelSerializer):
    currency = serializers.EmailField(source="from_account.currency", read_only=True)
    from_user = serializers.EmailField(source="from_account.user.email", read_only=True)
    to_user = serializers.EmailField(source="to_account.user.email")

    class Meta:
        model = Operation
        fields = ["from_user", "to_user", "amount", "currency"]
        read_only_fields = ["from_user"]
