from decimal import Decimal

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models, transaction

from common.models import CurrencyAwareModelMixin
from exchange.models import Rate
from .validators import validate_amount


class Account(CurrencyAwareModelMixin, models.Model):
    user = models.OneToOneField(
        get_user_model(), verbose_name="Пользователь", on_delete=models.CASCADE, related_name="account"
    )
    balance = models.DecimalField(
        verbose_name="Баланс", max_digits=19, decimal_places=4, default=0
    )

    class Meta:
        verbose_name = "Счет"
        verbose_name_plural = "счета"

    def __str__(self):
        return f"Счет пользователя {self.user}"


class Operation(models.Model):
    to_account = models.ForeignKey(
        Account,
        verbose_name="Получатель",
        on_delete=models.PROTECT,
        related_name="incoming",
        blank=False,
        null=False,
    )
    from_account = models.ForeignKey(
        Account,
        verbose_name="Отправитель",
        on_delete=models.PROTECT,
        related_name="outgoing",
        blank=False,
        null=False,
    )
    amount = models.DecimalField(
        verbose_name="Сумма",
        max_digits=19,
        decimal_places=4,
        default=0,
        help_text="В валюте отправителя",
        validators=[validate_amount],
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")

    class Meta:
        verbose_name = "Перевод"
        verbose_name_plural = "переводы"

    def __str__(self):
        if not self.pk:
            return "Новая операция"
        return f"{self.amount} {self.from_account.currency} от {self.from_account.user} к {self.to_account.user}"

    def clean(self):
        super(Operation, self).clean()
        if self.to_account_id is None or self.from_account_id is None:
            return

        if self.from_account == self.to_account:
            raise ValidationError({"to_account": "Вы не можете перевести деньги себе"})

        if self.from_account.balance < self.amount:
            raise ValidationError({"amount": "Недостаточно средств для проведения операции"})

    @classmethod
    def transfer(cls, from_account, to_account, amount):
        with transaction.atomic():
            obj = cls(from_account=from_account, to_account=to_account, amount=amount)
            obj.full_clean()
            obj.save()

            from_account = Account.objects.select_for_update().get(pk=from_account.pk)
            from_account.balance = from_account.balance - Decimal(amount)
            from_account.save()

            converted_amount = Rate.convert(from_account.currency, to_account.currency, Decimal(amount))
            to_account = Account.objects.select_for_update().get(pk=to_account.pk)
            to_account.balance += converted_amount
            to_account.save()

        return obj
