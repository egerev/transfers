from django.contrib import admin

from .models import Account, Operation


class AccountAdmin(admin.ModelAdmin):
    list_display = ("user", "currency", "balance")
    search_fields = ("user__email",)


class OperationAdmin(admin.ModelAdmin):
    list_display = ("to_account", "from_account", "amount")

admin.site.register(Account, AccountAdmin)
admin.site.register(Operation, OperationAdmin)