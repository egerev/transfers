# Generated by Django 3.0.2 on 2020-01-28 01:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_operation_created_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='currency',
            field=models.CharField(choices=[('EUR', 'Евро'), ('USD', 'Доллар США'), ('GBP', 'Фунт стерлингов'), ('RUB', 'Российский рубль'), ('BTC', 'Биткоин')], max_length=3, verbose_name='Валюта'),
        ),
        migrations.AlterField(
            model_name='account',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='account', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь'),
        ),
    ]
