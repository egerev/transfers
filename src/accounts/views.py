from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db.models import Q
from rest_framework import status
from rest_framework.generics import RetrieveAPIView, CreateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import Account, Operation
from .serializers import AccountSerializer, OperationSerializer, NewAccountSerializer

User = get_user_model()


class CreateAccountView(CreateAPIView):
    serializer_class = NewAccountSerializer


class AccountView(RetrieveAPIView):
    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user.account


class OperationViewSet(CreateModelMixin, ReadOnlyModelViewSet):
    queryset = Operation.objects.all()
    serializer_class = OperationSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        qs = super(OperationViewSet, self).get_queryset()
        qs = qs.filter(Q(to_account=self.request.user.account) | Q(from_account=self.request.user.account))
        return qs

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        to_user_account = User.objects.get(email=data["to_user"]).account
        from_user_account = request.user.account
        amount = data.get("amount")

        try:
            operation = Operation.transfer(from_user_account, to_user_account, amount)
        except ValidationError as ex:
            return Response(ex.error_dict, status=status.HTTP_400_BAD_REQUEST)

        headers = self.get_success_headers(serializer.data)
        serializer = self.get_serializer(operation)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
