Django==3.0.2
psycopg2==2.8.4
django-username-email==2.2.4
celery==4.4.0
requests==2.22.0
redis==3.3.11
djangorestframework==3.11.0