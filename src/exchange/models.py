from __future__ import annotations

from decimal import Decimal
from typing import Union

from django.core.exceptions import ValidationError
from django.db import models

from common.models import CurrencyAwareModelMixin
from .constants import BASE_CURRENCY


class Rate(CurrencyAwareModelMixin, models.Model):
    rate = models.DecimalField(
        verbose_name="Курс", max_digits=19, decimal_places=4, default=0, help_text=f"Относительно базовой валюты ({BASE_CURRENCY})"
    )
    multiplicity = models.PositiveSmallIntegerField(verbose_name="Кратность", help_text="Определяет относительно какой суммы в базовой валюте указан курс", default=1)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Последнее обновление")

    class Meta:
        verbose_name = "Курс"
        verbose_name_plural = "курсы"

    def __str__(self) -> str:
        return f"Курс {self.rate} {self.currency} к 1 {BASE_CURRENCY}"

    def convert_to_base(self, amount: Union[Decimal, int]) -> Decimal:
        return Decimal(amount) / self.rate * self.multiplicity

    def convert_from_base(self, amount: Union[Decimal, int]) -> Decimal:
        return Decimal(amount) * self.rate / self.multiplicity

    @classmethod
    def get_rate_or_raise_error(cls, currency: str) -> Rate:
        """
        Tries to get exchange rate from db for currency by it's name.

        Raises:
            ValidationError: When there is no exchange rate for this currency in db
        """
        try:
            rate = cls.objects.get(currency=currency)
        except cls.DoesNotExist:
            raise ValidationError(f"Отсутствует курс для валюты {currency}")
        return rate

    @classmethod
    def convert(cls, from_currency: str, to_currency: str, amount: Union[Decimal, int]) -> Decimal:
        """
        Converts amount from one currency to another through base currency

        Raises:
            ValidationError: When there is no exchange rate for some of the currencies, used in this conversation
        """
        if from_currency == to_currency:
            return amount

        if from_currency != BASE_CURRENCY:
            from_currency_rate = cls.get_rate_or_raise_error(from_currency)
            amount_in_base_currency = from_currency_rate.convert_to_base(amount)
        else:
            amount_in_base_currency = amount

        if to_currency != BASE_CURRENCY:
            to_currency_rate = cls.get_rate_or_raise_error(to_currency)
            amount_in_target_currency = to_currency_rate.convert_from_base(amount_in_base_currency)
        else:
            amount_in_target_currency = amount_in_base_currency

        return amount_in_target_currency

