from decimal import Decimal

from django.core.exceptions import ValidationError
from django.test import TestCase

from exchange.constants import BASE_CURRENCY
from ..models import Rate


class TestRateModelMethods(TestCase):
    def setUp(self):
        self.rate_rub = Rate.objects.create(currency=Rate.CURRENCY_RUB, rate=Decimal("68.5"))
        self.rate_usd = Rate.objects.create(currency=Rate.CURRENCY_USD, rate=Decimal("1.1"))

    def test_str(self):
        print(str(self.rate_rub))

    def test_convert_to_base(self):
        self.assertEqual(self.rate_rub.convert_to_base(137), Decimal(2))

    def test_convert_from_base(self):
        self.assertEqual(self.rate_rub.convert_from_base(2), Decimal(137))

    def test_convert_to_unknown_currency(self):
        with self.assertRaises(ValidationError):
            Rate.convert(Rate.CURRENCY_RUB, Rate.CURRENCY_BTC, 1000)

    def test_convert_to_another_currency(self):
        result = Rate.convert(Rate.CURRENCY_RUB, Rate.CURRENCY_USD, 1000)
        self.assertEqual(result, Decimal(1000) * Decimal("1.1") / Decimal("68.5"))

    def test_convert_equals_currencies(self):
        result = Rate.convert(Rate.CURRENCY_BTC, Rate.CURRENCY_BTC, 100)
        self.assertEqual(result, 100)

    def test_convert_rub_to_base(self):
        self.assertEqual(Rate.convert(Rate.CURRENCY_RUB, BASE_CURRENCY, 137), 2)

    def test_convert_base_to_rub(self):
        self.assertEqual(Rate.convert(BASE_CURRENCY, Rate.CURRENCY_RUB, 2), 137)


