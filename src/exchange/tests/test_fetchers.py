import json
import os
from decimal import Decimal
from unittest.mock import MagicMock, patch, Mock

import requests
from requests.exceptions import ConnectionError
from django.test import TestCase

from .. import rates_fetchers


def get_path(*args):
    path = os.path.join(os.path.dirname(__file__), *args)
    print(path)
    return path


def raise_error(*args, **kwargs):
    raise ConnectionError


class TestExchangeRatesApiFetcher(TestCase):
    @staticmethod
    def return_ok_response(*args, **kwargs):
        response = Mock()
        response.status_code = 200
        response.text = open(get_path("fixtures", "exchange_rates_ok.json"), 'r').read()
        return response

    @patch.object(rates_fetchers, "requests")
    def test_ok(self, mock_requests):
        mock_requests.get = MagicMock(side_effect=self.return_ok_response)
        res = rates_fetchers.fetch_exchange_rates_api()
        self.assertEqual(len(res), 3)
        self.assertEqual(res[0][0], "USD")
        self.assertEqual(res[0][1], Decimal("1.1005"))
        self.assertEqual(res[0][2], 1)

    @patch.object(rates_fetchers, "requests")
    def test_fail(self, mock_requests):
        mock_requests.get = MagicMock(side_effect=raise_error)
        res = rates_fetchers.fetch_exchange_rates_api()
        self.assertEqual(len(res), 0)


class TestBlockchainInfoFetcher(TestCase):
    @staticmethod
    def return_ok_response(*args, **kwargs):
        response = Mock()
        response.status_code = 200
        response.text = open(get_path("fixtures", "blockchain_info_ok.txt"), 'r').read()
        return response

    @patch.object(rates_fetchers, "requests")
    def test_ok(self, mock_requests):
        mock_requests.get = MagicMock(side_effect=self.return_ok_response)
        res = rates_fetchers.fetch_blockchain_info()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][0], "BTC")
        self.assertEqual(res[0][1], Decimal("0.11863094"))
        self.assertEqual(res[0][2], 1000)

    @patch.object(rates_fetchers, "requests")
    def test_fail(self, mock_requests):
        mock_requests.get = MagicMock(side_effect=raise_error)
        res = rates_fetchers.fetch_blockchain_info()
        self.assertEqual(len(res), 0)
