from django.contrib import admin

from .models import Rate


class RateAdmin(admin.ModelAdmin):
    list_display = ("currency", "rate", "multiplicity", "updated_at")

admin.site.register(Rate, RateAdmin)