import json
import logging
from decimal import Decimal
from typing import List, Tuple

import requests
from django.conf import settings
from requests import RequestException

from .models import Rate

logger = logging.getLogger(__name__)


def fetch_exchange_rates_api() -> List[Tuple]:
    try:
        result = requests.get(
            "https://api.exchangeratesapi.io/latest",
            timeout=(
                settings.EXCHANGE_RATES_CONNECT_TIMEOUT,
                settings.EXCHANGE_RATES_READ_TIMEOUT,
            ),
        )
    except RequestException as e:
        logger.error(e)
        return []

    data = json.loads(result.text, parse_float=lambda value: Decimal(value))

    rates: List[Tuple] = []

    for cur in Rate.CURRENCIES:
        if cur not in data["rates"]:
            logger.info(f"There is not exchange rate for {cur}")
            continue

        rates.append((cur, data["rates"][cur], 1))

    return rates


def fetch_blockchain_info() -> List[Tuple]:
    try:
        result = requests.get(
            "https://blockchain.info/tobtc?currency=EUR&value=1000",
            timeout=(
                settings.EXCHANGE_RATES_CONNECT_TIMEOUT,
                settings.EXCHANGE_RATES_READ_TIMEOUT,
            ),
        )
    except RequestException as e:
        logger.error(e)
        return []

    rate = Decimal(result.text)

    return [(Rate.CURRENCY_BTC, rate, 1000)]
