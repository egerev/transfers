import logging
from celery import shared_task

from .models import Rate
from .rates_fetchers import fetch_exchange_rates_api, fetch_blockchain_info
from .constants import BASE_CURRENCY

logger = logging.getLogger(__name__)


@shared_task(name="update_rates")
def updates_rates():
    rates = fetch_exchange_rates_api()
    rates += fetch_blockchain_info()

    received_rates = set()
    for currency, rate, multiplicity in rates:
        Rate.objects.update_or_create(currency=currency, defaults={"rate": rate, "multiplicity": multiplicity})
        received_rates.add(currency)

    logger.info(
        "Received exchange rates for currencies: {}".format(", ".join(received_rates))
    )

    not_received = set(Rate.CURRENCIES) - received_rates - set([BASE_CURRENCY])
    if not_received:
        logger.error(
            "Have not found exchange rates for currencies: {}".format(
                ", ".join(not_received)
            )
        )
